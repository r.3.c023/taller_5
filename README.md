# Taller de Algoritmos de Búsqueda y Ordenamiento

## Objetivos del Taller

- Comprender y diferenciar los métodos de búsqueda binaria y búsqueda lineal.
- Implementar y analizar el desempeño de los algoritmos de ordenamiento Quicksort y Mergesort.
- Comparar la eficiencia de estos métodos con algoritmos de ordenamiento más simples como el Bubble Sort.
- Realizar un profiling de los algoritmos para entender mejor su comportamiento en términos de tiempo y eficiencia de memoria.
- Aplicar los principios de resolución de problemas de dividir el problema, resolver partes individuales y unir los resultados, con especial énfasis en la recursividad.
- Generar una tabla comparativa de la complejidad algorítmica de cada método de búsqueda y ordenamiento utilizado en el taller.

## Materiales Necesarios

- Un entorno de programación que soporte Python (por ejemplo, Jupyter Notebook, Google Colab, o un IDE como PyCharm o VS Code).
- Acceso a recursos de documentación de Python, como la documentación oficial o tutoriales en línea.

## Parte 1: Búsqueda Lineal vs. Búsqueda Binaria

### Actividad 1: Implementación de Búsqueda Lineal

1. **Descripción Teórica**: Breve descripción de cómo funciona la búsqueda lineal.
2. **Implementación**: Escribe una función en Python que implemente la búsqueda lineal.
3. **Prueba**: Utiliza esta función para buscar elementos en una lista no ordenada de números y registra el número de comparaciones realizadas.

### Actividad 2: Implementación de Búsqueda Binaria

1. **Descripción Teórica**: Explicación de la búsqueda binaria y su requerimiento de trabajar sobre listas ordenadas.
2. **Implementación**: Escribe una función en Python que implemente la búsqueda binaria.
3. **Prueba**: Ordena la lista anterior y utiliza la búsqueda binaria para encontrar los mismos elementos, registrando el número de comparaciones.

### Preguntas de Comparación

- ¿En qué casos es más eficiente cada tipo de búsqueda?
- ¿Cómo afecta el tamaño de la lista al rendimiento de cada algoritmo de búsqueda?

## Parte 2: Comparación de Algoritmos de Ordenamiento

### Actividad 3: Implementación de Quicksort y Mergesort

1. **Descripción Teórica**: Explicar cómo funcionan el Quicksort y el Mergesort, enfocándose en cómo estos algoritmos aplican los principios de dividir el problema, resolver de manera recursiva, y unir los resultados.
2. **Implementación**:
   - Escribe funciones en Python para cada algoritmo.
   - Asegúrate de comentar el código para indicar las partes clave de cada algoritmo.

### Actividad 4: Implementación de Bubble Sort

1. **Descripción Teórica**: Descripción del Bubble Sort y por qué generalmente es menos eficiente.
2. **Implementación**: Escribe una función para Bubble Sort.

### Actividad 5: Comparación de Rendimiento y Profiling

1. **Creación de Lista para Pruebas**: Genera una lista de 2000 datos aleatorios para las pruebas de ordenamiento.
2. **Prueba**: Ordena esta lista utilizando los tres algoritmos.
3. **Registro de Tiempos y Profiling**: Utiliza la biblioteca `time` de Python para medir el tiempo de ejecución y herramientas como `cProfile` para realizar un análisis de perfil del rendimiento de cada algoritmo.

### Preguntas de Reflexión

- ¿Cómo varía el rendimiento de cada algoritmo en función del tipo de datos de entrada?
- ¿Por qué algunos algoritmos funcionan mejor en ciertos tipos de listas que en otros?

## Parte 3: Tabla de Complejidad Algorítmica

- **Generación de la Tabla**: Crea una tabla que liste todos los algoritmos utilizados en el taller, tanto de búsqueda como de ordenamiento, indicando su complejidad algorítmica en el mejor, promedio y peor caso. La tabla deberá estar organizada del más eficiente al menos eficiente según la complejidad en el caso promedio.

## Reporte

- **Elaboración de Reporte**: Al finalizar el taller, deberan reportar como de constumbre en latex y el codigo debera estar en git
